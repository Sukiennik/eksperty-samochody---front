import React from 'react';
import './App.css';
import { Nav, Navbar, NavbarBrand } from "react-bootstrap";
import { Route, Router } from 'react-router-dom';
import Cars from "./components/cars/Cars";
import AnimalForm from "./components/animals/AnimalForm";
import { createBrowserHistory } from "history";
import Animals from "./components/animals/Animals";

const history = createBrowserHistory();

const App: React.FC = () => {

    return (
        <Router history={history}>
            <div>
                <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                    <NavbarBrand href="/cars">Systemy Ekspertowe - Samochody</NavbarBrand>
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="mr-auto">
                            <Nav.Link href="/cars">Samochody - Prolog</Nav.Link>
                            <Nav.Link href="/animals">Zwierzęta - Sieci Bayesa</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
                <div className="App">
                    <Route exact path='/' component={Cars}/>
                    <Route exact path='/cars' component={Cars}/>
                    <Route exact path='/animals' component={Animals}/>
                </div>
            </div>
        </Router>
    );
};

export default App;
