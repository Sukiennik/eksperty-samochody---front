export interface AnimalQuestion {
    question: string,
    filterId: string,
    filterAnswers: FilterAnswer[];
}

export interface FilterAnswer {
    label: string,
    id: string
}

export interface AnimalFilter {
    Id: string,
    Value: string,
}

export interface AnimalParams {
    SearchedNodeId: string,
    NodesFilters: AnimalFilter[]
}

export interface AnimalResult {
    Name: string,
    Probability: number
}

export const initialAnimalParams: AnimalParams = { SearchedNodeId: 'Zwierze', NodesFilters: [] };

export const animalQuestions: AnimalQuestion[] = [
    {
        question: 'Jaki kolor ma zwierze?',
        filterId: 'kolor',
        filterAnswers: [
            { label: 'Biały', id: 'Bialy' },
            { label: 'Brązowy', id: 'Brazowy' },
            { label: 'Biało - Czarny', id: 'bialoCzarny' },
            { label: 'Szary', id: 'szary' },
            { label: 'Różowy', id: 'rozowy' },
            { label: 'Żółty', id: 'zolty' },
        ]
    },
    {
        question: 'Pod względem spożywanego pokarmu zwierze jest?',
        filterId: 'Pozywienie',
        filterAnswers: [
            { label: 'Roślinożerne', id: 'roslinozerny' },
            { label: 'Mięsożerne', id: 'miesozerny' },
            { label: 'Wszystkożerne', id: 'wszystkozerny' },
        ]
    },
    {
        question: 'W jak licznych grupach osobników wystepuje zwierze?',
        filterId: 'Stadnosc',
        filterAnswers: [
            { label: 'Samotnik', id: 'samotnik' },
            { label: 'Grupa', id: 'grupa' },
            { label: 'Stado', id: 'stado' },
        ]
    },
    {
        question: 'Jaki ogon ma zwierze?',
        filterId: 'Ogon',
        filterAnswers: [
            { label: 'Brak', id: 'brak' },
            { label: 'Długi', id: 'dlugi' },
            { label: 'Krótki', id: 'krotki' },
        ]
    },
    {
        question: 'Jakie poroże ma zwierzę?',
        filterId: 'Poroze',
        filterAnswers: [
            { label: 'Brak', id: 'brak' },
            { label: 'Małe', id: 'male' },
            { label: 'Duże', id: 'duze' },
        ]
    },
    {
        question: 'Jakie pazury ma zwierzę?',
        filterId: 'Pazury',
        filterAnswers: [
            { label: 'Brak', id: 'brak' },
            { label: 'Krótkie', id: 'krotkie' },
            { label: 'Długie', id: 'dlugie' },
        ]
    },
    {
        question: 'Jakie kły (zęby) ma zwierzę?',
        filterId: 'klyZeby',
        filterAnswers: [
            { label: 'Brak', id: 'brak' },
            { label: 'Krótkie', id: 'krotkie' },
            { label: 'Długie', id: 'dlugie' },
        ]
    },
];
