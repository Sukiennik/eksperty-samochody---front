export interface Question {
    Text: string,
    AllowMultipleAnswers: boolean,
    Answers: Answer[]
    paramName: string
}

export interface Answer {
    Text: string
    paramName: string
}

export interface CarResult {
    Code: string
    Name: string
    FuelType: FuelType,
    HasFourWheelsDrive: boolean
    Type: VehicleType
    Class: number
    Price: number
    MaxNumberOfPassengers: number
    Acceleration: number
    FuelConsumption: number
    Reliability: number
    Safety: number
}

export interface SearchQueryParams {
    CityUsage: boolean,
    EverydayUsage: boolean,
    HolidayTripsUsage: boolean,
    BusinessTripsUsage: boolean,
    MountainRoadsUsage: boolean,
    SafetyImportance: number,
    PerformanceImportance: number,
    ReliabilityImportance: number,
    EconomyImportance: number,
    MaximumPrice: number
    Type: VehicleType | number,
    UsageFrequency: UsageFrequency,
    WeddingCar: boolean,
    Ecologic: boolean,
}

export const vehicleTypeMap: string[] = ['', 'Mały', 'Średni', 'VAN Rodzinny', 'SUV', 'Dostawczak'];
export const fuelTypeMap: string[] = ['', 'Diesel', 'Benzyna', 'Elektryczne', 'Hybryda'];
export const usageFrequency: string[] = ['', 'Codziennie', 'Parę razy na tydzień', 'Raz na tydzień lub mniej'];

export enum VehicleType {
    SMALL = 1,
    MEDIUM,
    VAN,
    SUV,
    CARGO
}

export enum UsageFrequency {
    EVERYDAY = 1,
    FEW,
    ONCE_OR_LESS
}

export enum FuelType {
    DIESEL = 1,
    PETROl,
    ELECTRIC,
    HYBRID
}