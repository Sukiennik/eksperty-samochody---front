import {Question} from "./QuestionAsnwerModel";

export const booleanAnswers: string[] = ['Tak', 'Nie'];
export const ratingAnswers: string[] = ['1', '2', '3', '4', '5'];

const PURPOSE_Q = 'Do jakich celów zamierzasz użytkować pojazd?';
const USAGE_Q = 'Jak często korzystasz z pojazdu?';
const TYPE_Q = 'Jakiego pojazdu poszukujesz?';
const PRICE_Q = 'Do jakiej ceny poszukujesz auta?';
const MOUNTAIN_Q = 'Czy zdarza Ci się jeździć po górskich lub nieasfaltowanych drogach?';
const CITY_Q = 'Czy jeździsz autem po mieście?';
const SAFETY_Q = 'Jak ważne jest dla Ciebie bezpieczeństwo?';
const PERFORMANCE_Q = 'Jak ważne są dla Ciebie osiągi?';
const RELIABILITY_Q = 'Jak ważna jest dla Ciebie bezwarayjność?';
const CONSUMPTION_Q = 'Jak ważne są dla Ciebie koszty eksploatacji?';
const ECONOMIC_Q = 'Czy ważna jest dla Ciebie ekologiczność';
const WEDDING_Q = 'Czy poszukujesz auta na ślub?';

export const decorate = (questions: Question[]) => {
    return questions.forEach(question => {
        switch (question.Text) {
            case PURPOSE_Q:
                question.Answers.forEach(answer => {
                    switch (answer.Text) {
                        case 'Dojazdy do pracy i codzienne sprawy':
                            answer.paramName = 'EverydayUsage';
                            break;
                        case 'Wyjazdy na wakacje':
                            answer.paramName = 'HolidayTripsUsage';
                            break;
                        case 'Wyjazdy biznesowe':
                            answer.paramName = 'BusinessTripsUsage';
                            break;
                        default:
                            return
                    }
                });
                break;
            case USAGE_Q:
                question.paramName = 'UsageFrequency';
                break;
            case TYPE_Q:
                question.paramName = 'Type';
                break;
            case PRICE_Q:
                question.paramName = 'MaximumPrice';
                break;
            case MOUNTAIN_Q:
                question.paramName = 'MountainRoadsUsage';
                break;
            case CITY_Q:
                question.paramName = 'CityUsage';
                break;
            case SAFETY_Q:
                question.paramName = 'SafetyImportance';
                break;
            case PERFORMANCE_Q:
                question.paramName = 'PerformanceImportance';
                break;
            case RELIABILITY_Q:
                question.paramName = 'ReliabilityImportance';
                break;
            case CONSUMPTION_Q:
                question.paramName = 'EconomyImportance';
                break;
            case ECONOMIC_Q:
                question.paramName = 'Ecologic';
                break;
            case WEDDING_Q:
                question.paramName = 'WeddingCar';
                break;
            default:
                return

        }
    })
};