import React from 'react'
import {Form} from "react-bootstrap";
import {CarResult, fuelTypeMap, vehicleTypeMap} from "../../models/cars/QuestionAsnwerModel";
import Rating from "react-rating";

interface CarInfoProps {
    car: CarResult,
    children?: React.ReactNode
}

const CarInfo: React.FC<CarInfoProps> = ({car}) => {
    return (
        <>
            <Form.Label column={true}>Klasa:</Form.Label>
            <Form.Control plaintext readOnly defaultValue={car.Class}/>
            <Form.Label column={true}>Typ pojazdu:</Form.Label>
            <Form.Control plaintext readOnly defaultValue={vehicleTypeMap[car.Type]}/>
            <Form.Label column={true}>Typ Paliwa:</Form.Label>
            <Form.Control plaintext readOnly defaultValue={fuelTypeMap[car.FuelType]}/>
            <Form.Label column={true}>Napęd 4x4:</Form.Label>
            <Form.Control plaintext readOnly defaultValue={car.HasFourWheelsDrive ? 'Tak' : 'Nie'}/>
            <Form.Label column={true}>Liczba pasażerów:</Form.Label>
            <Form.Control plaintext readOnly defaultValue={car.MaxNumberOfPassengers}/>
            <Form.Label column={true}>Przyspieszenie:</Form.Label>
            <Form.Control plaintext readOnly defaultValue={car.Acceleration}/>
            <Form.Label column={true}>Spalanie:</Form.Label>
            <Form.Control plaintext readOnly defaultValue={car.FuelConsumption}/>
            <Form.Label column={true}>Bezpieczeństwo:</Form.Label>
            <Rating readonly={true} initialRating={car.Safety}/>
            <Form.Label column={true}>Bezawaryjność:</Form.Label>
            <Rating readonly={true} initialRating={car.Reliability}/>
            <Form.Label column={true}>Cena:</Form.Label>
            <Form.Control plaintext readOnly defaultValue={car.Price}/>
        </>
    )
};

export default CarInfo