import React from "react";
import CarForm from "./CarForm";
import CarCards from "./CarCards";
import { Accordion, Card } from "react-bootstrap";
import { CarResult } from "../../models/cars/QuestionAsnwerModel";


const Cars: React.FC<{}> = () => {

    const style = {
        root: {
            width: '75%'
        },
        card: {
            backgroundColor: '#343a40',
            width: '100%',
        }
    };
    const [currentCars, setCurrentCars] = React.useState({} as CarResult[]);
    const handleCarResults = (value: CarResult[]) => {
        setCurrentCars(value);
    };

    return (
        <Accordion style={style.root} defaultActiveKey="0">
            <Card style={style.card}>
                <Accordion.Toggle variant='link' as={Card.Header} eventKey="0">
                    <h3>Wyszukaj samochód!</h3>
                </Accordion.Toggle>
                <Accordion.Collapse eventKey="0">
                    <Card.Body>
                        <CarForm updateCarResults={handleCarResults}/>
                    </Card.Body>
                </Accordion.Collapse>
            </Card>
            <Card style={style.card}>
                <Accordion.Toggle as={Card.Header} eventKey="1">
                    <h3>Znalezione samochody</h3>
                </Accordion.Toggle>
                <Accordion.Collapse eventKey="1">
                    <Card.Body>
                        <CarCards cars={currentCars}/>
                    </Card.Body>
                </Accordion.Collapse>
            </Card>
        </Accordion>
    )
};

export default Cars
