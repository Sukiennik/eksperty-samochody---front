import React, {useEffect, useState} from "react";
import ReactDOM from 'react-dom';
import {Answer, CarResult, Question, SearchQueryParams} from "../../models/cars/QuestionAsnwerModel";
import {booleanAnswers, decorate, ratingAnswers} from "../../models/cars/ModelDecorator";
import {Button, Form} from "react-bootstrap";
import Rating from "react-rating";

const style = {
    checkboxes: {
        display: 'flex',
        justifyContent: 'center'
    },
};

interface CarFormProps {
    updateCarResults: Function,
    children?: React.ReactNode
}

const CarForm: React.FC<CarFormProps> = ({updateCarResults}) => {
    const useMountEffect = (fun: any) => useEffect(fun, []);
    const [questionsData, setQuestionData] = useState([] as Question[]);
    const [searchParams, setSearchParams] = useState({} as SearchQueryParams);
    let carForm: any;

    useMountEffect(() => {
        fetch('http://localhost:8150/questions')
            .then(res => res.json())
            .then((result: Question[]) => {
                decorate(result);
                setQuestionData(result);
            })
    });

    const handleSubmit = (event: any) => {
        event.preventDefault();
        console.log(searchParams);
        fetch('http://localhost:8150/questions/results', {
            method: 'post',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(searchParams)
        } as unknown as RequestInit)
            .then(res => res.json())
            .then((results: CarResult[]) => {
                console.log(results);
                updateCarResults(results)
            })
    };

    const handleClear = (event: any) => {
        (ReactDOM.findDOMNode(carForm) as HTMLFormElement).reset();
        setSearchParams({} as SearchQueryParams);
        updateCarResults({})
    };

    const handleAnswerChange = (event: React.SyntheticEvent<HTMLInputElement>, param: string, type?: 'numeric' | 'checkbox') => {
        if (type === 'numeric') {
            let newNumber = Number.parseInt(event.currentTarget.value);
            setSearchParams(prevState => {
                return {...prevState, [param]: isNaN(newNumber) ? null : newNumber}
            });
        } else if (type === 'checkbox') {
            let newAnswer = event.currentTarget.checked;
            setSearchParams(prevState => {
                return {...prevState, [param]: newAnswer}
            });
        } else {
            let newAnswer = event.currentTarget.value;
            setSearchParams(prevState => {
                return {...prevState, [param]: newAnswer ? newAnswer : null}
            });
        }
    };

    function renderAnswer(question: Question) {
        let selectRatingAnswer = !question.AllowMultipleAnswers && question.Answers.length === 5
            && ratingAnswers.includes(question.Answers[0].Text)
        let selectStringAnswer = !question.AllowMultipleAnswers && question.Answers.length > 0
            && !booleanAnswers.includes(question.Answers[0].Text);
        let selectBooleanAnswer = !question.AllowMultipleAnswers && question.Answers.length === 2
            && booleanAnswers.includes(question.Answers[0].Text);
        let numberAnswer = question.Answers.length === 0;
        let multipleAnswers = question.AllowMultipleAnswers && question.Answers.length > 0;
        if (numberAnswer) {
            return (
                <Form.Control onChange={(event: any) => handleAnswerChange(event, question.paramName, 'numeric')}
                              type="number"/>
            )
        }
        if (selectRatingAnswer) {
            return (
                <Rating start={-1} initialRating={(searchParams as any)[question.paramName]} onChange={(value: number) => {
                    const event = {currentTarget: {value: value}} as unknown as React.SyntheticEvent<HTMLInputElement>;
                    handleAnswerChange(event, question.paramName)
                }}/>
            )
        }
        if (selectStringAnswer) {
            return (
                <Form.Control as="select"
                              onChange={(event: any) => handleAnswerChange(event, question.paramName)}>
                    <option label=" "/>
                    {question.Answers.map((value: Answer, index) => {
                        return <option key={index} value={index + 1}>{value.Text}</option>
                    })}
                </Form.Control>
            )
        }
        if (selectBooleanAnswer) {
            return (
                <Form.Control as="select"
                              onChange={(event: any) => handleAnswerChange(event, question.paramName)}>
                    <option label=" "/>
                    {question.Answers.map((value: Answer, index) => {
                        return <option key={index}
                                       value={value.Text === booleanAnswers[0] ? 'true' : 'false'}>{value.Text}</option>
                    })}
                </Form.Control>
            )
        }
        if (multipleAnswers) {
            return (
                <div style={style.checkboxes}>
                    {question.Answers.map((answer: Answer, index) => {
                        return <Form.Check
                            onChange={(event: any) => handleAnswerChange(event, answer.paramName, 'checkbox')}
                            key={index}
                            inline label={answer.Text} type='checkbox' id={`inline-${index}`}/>
                    })}
                </div>
            )
        }
    }

    function renderFormElement(question: Question, index: number) {
        let control = renderAnswer(question);
        return (
            <Form.Group key={index}>
                <Form.Label column={true}>{question.Text}</Form.Label>
                {control}
            </Form.Group>
        )
    }

    return (
        <Form id="carForm" onSubmit={(event: any) => handleSubmit(event)} ref={(form: any) => carForm = form}>
            {questionsData.map((question, index) => {
                return renderFormElement(question, index)
            })}
            <Button variant="primary" type="submit" block>Wyszukaj</Button>
            <Button onClick={handleClear} variant="secondary" type="button" block>Wyczyść</Button>
        </Form>
    )
};

export default CarForm