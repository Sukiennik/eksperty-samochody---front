import React from 'react'
import {Card, CardColumns} from "react-bootstrap";
import {CarResult} from "../../models/cars/QuestionAsnwerModel";
import CarInfo from "./CarInfo";

interface CarCardsProps {
    cars: CarResult[],
    children?: React.ReactNode
}

const CarCards: React.FC<CarCardsProps> = ({cars}) => {
    console.log(cars);

    return (
        <>
            {cars.length > 0 ?
                <CardColumns>
                    {cars.map((car: CarResult, index) => {
                        return (
                            <Card key={index} bg={"secondary"}>
                                <Card.Header>
                                    <h4>{car.Name}</h4>
                                </Card.Header>
                                <Card.Body>
                                    <Card.Title>{car.Code}</Card.Title>
                                    <Card.Text>
                                        <CarInfo car={car}/>
                                    </Card.Text>
                                </Card.Body>
                            </Card>
                        )
                    })}
                </CardColumns>
                :
                <h2>Brak wyników!</h2>
            }
        </>
    )
};

export default CarCards