import React from "react";
import { Accordion, Card } from "react-bootstrap";
import AnimalsResults from "./AnimalResults";
import AnimalForm from "./AnimalForm";
import { AnimalResult } from "../../models/animals/AnimalsModel";


const Animals: React.FC<{}> = () => {
    const style = {
        root: {
            width: '75%'
        },
        card: {
            backgroundColor: '#343a40',
            width: '100%',
        }
    };

    const [animalsResult, setAnimalsResult] = React.useState({} as AnimalResult[]);
    const handleAnimalResults = (value: AnimalResult[]) => {
        setAnimalsResult(prevState => value);
    };

    return (
        <Accordion style={style.root} defaultActiveKey="0">
            <Card style={style.card}>
                <Accordion.Toggle variant='link' as={Card.Header} eventKey="0">
                    <h3>Wyszukaj zwierzę!</h3>
                </Accordion.Toggle>
                <Accordion.Collapse eventKey="0">
                    <Card.Body>
                        <AnimalForm updateAnimalResults={handleAnimalResults}/>
                    </Card.Body>
                </Accordion.Collapse>
            </Card>
            <Card style={style.card}>
                <Accordion.Toggle as={Card.Header} eventKey="1">
                    <h3>Wyniki</h3>
                </Accordion.Toggle>
                <Accordion.Collapse eventKey="1">
                    <Card.Body>
                        <AnimalsResults animals={animalsResult}/>
                    </Card.Body>
                </Accordion.Collapse>
            </Card>
        </Accordion>
    )
};

export default Animals
