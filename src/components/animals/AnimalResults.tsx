import React from 'react'
import { Card, CardColumns, Form } from "react-bootstrap";
import { AnimalResult } from "../../models/animals/AnimalsModel";

interface AnimalResultProps {
    animals: AnimalResult[],
    children?: React.ReactNode
}

const AnimalsResults: React.FC<AnimalResultProps> = ({ animals }) => {

    const [values, setValues] = React.useState(animals);
    React.useEffect(() => {
        setValues(animals)
    }, [animals]);

    return (
        <>
            {values.length > 0 ?
                <CardColumns>
                    {values.map((animal: AnimalResult, index) => {
                        return (
                            <Card key={index} bg={"secondary"}>
                                <Card.Header>
                                    <h4>{animal.Name}</h4>
                                </Card.Header>
                                <Card.Body>
                                    <Card.Title>
                                        <Form.Label column={true}>Szansa:</Form.Label>
                                        <Form.Control plaintext readOnly value={(animal.Probability * 100).toString()}/>
                                    </Card.Title>
                                </Card.Body>
                            </Card>
                        )
                    })}
                </CardColumns>
                :
                <h2>Brak wyników!</h2>
            }
        </>
    )
};

export default AnimalsResults
