import React, { useState } from "react";
import { Button, Form } from "react-bootstrap";
import { CarResult } from "../../models/cars/QuestionAsnwerModel";
import ReactDOM from "react-dom";
import {
    AnimalFilter,
    AnimalQuestion,
    animalQuestions,
    FilterAnswer,
    initialAnimalParams
} from "../../models/animals/AnimalsModel";

interface AnimalsFormProps {
    updateAnimalResults: Function,
    children?: React.ReactNode
}

const AnimalForm: React.FC<AnimalsFormProps> = ({ updateAnimalResults }) => {

    const initialState = Object.assign({}, initialAnimalParams);
    const [animalsParams, setAnimalsParams] = useState(initialState);
    let animalsForm: any;

    const handleSubmit = (event: any) => {
        event.preventDefault();
        fetch('http://localhost:8150/animals', {
            method: 'post',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(animalsParams)
        } as unknown as RequestInit)
            .then(res => res.json())
            .then((results: CarResult[]) => {
                console.log(results);
                updateAnimalResults(results)
            })
    };

    const handleClear = (event: any) => {
        (ReactDOM.findDOMNode(animalsForm) as HTMLFormElement).reset();
        setAnimalsParams({ SearchedNodeId: 'Zwierze', NodesFilters: [] });
        updateAnimalResults({})
    };

    const handleAnswerChange = (event: React.SyntheticEvent<HTMLInputElement>, filterId: string) => {
        let newAnswer = event.currentTarget.value;
        let updatedFilter: AnimalFilter = {
            Id: filterId,
            Value: newAnswer
        };
        setAnimalsParams(prevState => {
            const filter = prevState.NodesFilters.find(filter => filter.Id === filterId);
            if (filter && newAnswer.length > 0) {
                Object.assign(filter, updatedFilter)
            } else if (filter) {
                prevState.NodesFilters.splice(prevState.NodesFilters.indexOf(filter), 1)
            } else {
                prevState.NodesFilters.push(updatedFilter)
            }
            return { ...prevState }
        });
    };

    function renderFormElement(question: AnimalQuestion, index: number) {
        return (
            <Form.Group key={index}>
                <Form.Label column={true}>{question.question}</Form.Label>
                <Form.Control as="select"
                              onChange={(event: any) => handleAnswerChange(event, question.filterId)}>
                    <option label=" "/>
                    {question.filterAnswers.map((value: FilterAnswer, index) => {
                        return <option key={index} value={value.id}>{value.label}</option>
                    })}
                </Form.Control>
            </Form.Group>
        )
    }

    return (
        <Form id="animalsForm" onSubmit={(event: any) => handleSubmit(event)} ref={(form: any) => animalsForm = form}>
            {animalQuestions.map((question, index) => {
                return renderFormElement(question, index)
            })}
            <Button variant="primary" type="submit" block>Wyszukaj</Button>
            <Button onClick={handleClear} variant="secondary" type="button" block>Wyczyść</Button>
        </Form>
    )
};

export default AnimalForm
